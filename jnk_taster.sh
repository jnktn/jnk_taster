#!/bin/bash

# Copyright 2022-2023 https://jnktn.tv
# Release under the terms of BSD-3-Clause (see LICENSE)

# set default value for the parameters
echo "jnk_tmp_dir = ${jnk_tmp_dir=/tmp}"
echo "jnk_output_dir = ${jnk_output_dir=/var/www/videos/previews}"
echo "jnk_stream_url = ${jnk_stream_url=https://owncast.jnktn.tv/hls/0/stream.m3u8}"
# end screen video must be in the same resolution as the incoming stream
echo "jnk_end_screen = ${jnk_end_screen=end_screen_1080p_30fps.mp4}"
echo "jnk_threads_count = ${jnk_threads_count=2}"


date=$(date "+%d.%m_%H.%M.%S")

for i in {0..4}
do
  command rm -fr "${jnk_tmp_dir}/recorded_cropped.mp4" "${jnk_tmp_dir}/recorded_full.mp4" "${jnk_tmp_dir}/recorded_merged.mp4"
  command ffmpeg -i "${jnk_stream_url}" -nostdin -c:v copy -t 00:00:10 "${jnk_tmp_dir}/recorded_full.mp4"
  command ffmpeg -i "${jnk_tmp_dir}/recorded_full.mp4" -nostdin -threads "${jnk_threads_count}" -c:v copy -t 00:00:07 -an "${jnk_tmp_dir}/recorded_cropped.mp4"
  command ffmpeg -i "${jnk_tmp_dir}/recorded_cropped.mp4" -r 30 -i "$(dirname "$0")/${jnk_end_screen}" -nostdin -threads "${jnk_threads_count}" -profile:v high -level:v 3.1 -filter_complex "[0:v] [1:v] concat=n=2:v=1:a=0 [v]" -map "[v]" "${jnk_tmp_dir}/recorded_merged.mp4"
  command ffmpeg -i "${jnk_tmp_dir}/recorded_merged.mp4" -i "${jnk_tmp_dir}/recorded_full.mp4" -nostdin -threads "${jnk_threads_count}" -map '0:v:0' -map '1:a:0' -c copy "${jnk_output_dir}/${date}_${i}.mp4"
  sleep 5
done

# final cleanup
command rm -fr "${jnk_tmp_dir}/recorded_cropped.mp4" "${jnk_tmp_dir}/recorded_full.mp4" "${jnk_tmp_dir}/recorded_merged.mp4"