# jnk_taster

Just a simple script to be executed as a cron job (or manually) for generating 5 short clips of the current show on jnktn.tv

The generated clips are 10 sec long each and include 3 sec ending screen.
